//
//  OMDBClient.swift
//  omdbPL2
//
//  Created by Luis Marcelino on 17/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import Foundation

class OMDBClient {
    static func searchMoviesWith(searchQuery:String) -> [Movie]? {
        guard let url = NSURL(string: "http://www.omdbapi.com/?s=" + searchQuery) else {
            return nil;
        }
        let urlSession = NSURLSession.sharedSession()
        let task = urlSession.dataTaskWithURL(url) { (data, response, error) -> Void in
            if let d = data {
                do {
                    
                    if let searchDic = try NSJSONSerialization.JSONObjectWithData(d, options: NSJSONReadingOptions.AllowFragments) as? [String:AnyObject] {
                        if let moviesArray = searchDic["Search"] as? [AnyObject] {
                            for jsonMovie in moviesArray {
                                let title = jsonMovie["Title"] as! String
                                let poster = jsonMovie["Poster"] as! String
                                
                                print(title + " > " + poster);
                                
                            }
                        }
                    }
                }
                catch {
                    
                }
                
            }
        }
        task.resume()
        
        return [Movie]()
    }
}